﻿using MC.Modeles;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MC.Controllers
{
    public class BoisonsController : Controller
    {
        private BoisonContext db = new BoisonContext();

        // GET: Boisons
        public ActionResult Index()
        {
            return View(db.Boisons.ToList());
        }

        // GET: Boisons/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Boison boison = db.Boisons.Find(id);
            if (boison == null)
            {
                return HttpNotFound();
            }
            return View(boison);
        }

        // GET: Boisons/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Boisons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BoisonId,Type")] Boison boison)
        {
            if (ModelState.IsValid)
            {
                db.Boisons.Add(boison);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(boison);
        }

        // GET: Boisons/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Boison boison = db.Boisons.Find(id);
            if (boison == null)
            {
                return HttpNotFound();
            }
            return View(boison);
        }

        // POST: Boisons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BoisonId,Type")] Boison boison)
        {
            if (ModelState.IsValid)
            {
                db.Entry(boison).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(boison);
        }

        // GET: Boisons/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Boison boison = db.Boisons.Find(id);
            if (boison == null)
            {
                return HttpNotFound();
            }
            return View(boison);
        }

        // POST: Boisons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Boison boison = db.Boisons.Find(id);
            db.Boisons.Remove(boison);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
