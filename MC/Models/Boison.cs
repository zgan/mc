﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace MC.Modeles
{
    public class Boison
    {
        public Boison()
        {

        }
        [Key]
        [Column(Order = 0)]
        public int BoisonId { get; set; }
        public string Type { get; set; }

        //  public virtual Commande Commande { get; set; }

    }
}
/*public class The:Boison
{
     public The()
    {
       base.Type = "The";
    }
}
public class Chocolat : Boison
{

    public Chocolat()
    {
        Type = "Chocolat";
    }
}
public class Cafe: Boison
{
    public Cafe()
    {
        Type = "Cafe";
    }
}*/
