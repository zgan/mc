﻿using MC.Modeles;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
public class Commande
{
    [Key]
    public int CommandeId { get; set; }
    [ForeignKey("Boison")]
    public int BoisonId { get; set; }
    public virtual Boison Boison { get; set; }
    public int Quantite { get; set; }

    public bool Mug { get; set; }
    public bool Badge { get; set; }
}