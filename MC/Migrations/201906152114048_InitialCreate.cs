namespace MC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Boisons",
                c => new
                    {
                        BoisonId = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.BoisonId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Boisons");
        }
    }
}
