namespace MC.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BoisonContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "BoisonContext";
        }

        protected override void Seed(BoisonContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            /*   context.Boisons.AddOrUpdate(
              p => p.Type,
                new Boison { Type = "Th�" },
                new Boison { Type = "Caf�" },
                new Boison { Type = "Chocolat" }*/
         //   );
            //
        }
    }
}
